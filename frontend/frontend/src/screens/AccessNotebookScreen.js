import React from 'react';

export default function AccessNotebookScreen(props){
  const id = props.match.params.id
  if(!id){
    return (
      <h1>Notebook inexistente</h1>
    )
  }
  return (
    <h1>Accessing notebook with id {id}</h1>
  )
}
