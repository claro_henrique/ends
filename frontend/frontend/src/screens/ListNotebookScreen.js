import React from 'react';
import Notebook from '../components/Notebook';
import data from '../data';

export default function NotebookScreen(props){
  return (
    <div className="row center">
    {data.notebooks.map((notebook) => (
        <Notebook key={notebook._id} notebook={notebook}></Notebook>
    ))}
    </div>
  )
}