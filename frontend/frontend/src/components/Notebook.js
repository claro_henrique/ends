import React from 'react';

export default function Notebook(props){
  const {notebook} = props;
  return (
  <div className="card">
    <h2>{notebook.name}</h2>
    <a href={`/notebook/access/${notebook._id}`}>access</a>
    <a href={`/notebook/delete/${notebook._id}`}>delete</a> 
  </div>
  )
}
