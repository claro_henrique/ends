
import { BrowserRouter, Route } from 'react-router-dom';
import AccessNotebookScreen from './screens/AccessNotebookScreen.js';
import HomeScreen from './screens/HomeScreen.js';
import ListNotebookScreen from './screens/ListNotebookScreen.js';


function App() {
  return (
    <BrowserRouter>
    <div className="grid-container">
            <header className="row">
                <div>
                    <a className="brand" href="/">Swirl Online</a>
                </div>
                <div>
                    <a href="/"> Home </a>
                    <a href="/notebook"> Notebooks </a>
                    <a href="https://www.hpcshelf.org/" target="_blank" rel="noreferrer"> About HPC Shelf </a>
                    <a href="xureque.html">xureque</a>
                </div>
            </header>

            <main>
              <Route path="/" component={HomeScreen} exact></Route>
              <Route path="/notebook" component={ListNotebookScreen} exact></Route>
              <Route path="/notebook/access/:id" component={AccessNotebookScreen} exact></Route>

               
            </main>

            <footer className="row center">
                All rights reserved
            </footer>
        </div>
    </BrowserRouter>
  );
}

export default App;
